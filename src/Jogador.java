import java.awt.Image;

public class Jogador {

	private String nome;
	
	
	public Jogador(String nome, String sexo, Image foto, String nacionalidade, Image bandeira) {
		this.nome = nome;
		
	}
	
	public Jogador() {
		this.nome = "";
		
	}
	
	public String getNome() {
		return this.nome;
	}
	
	
	public void setNome(String nome) {
		this.nome = nome;
	}
	
}
