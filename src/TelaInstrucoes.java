import java.awt.Canvas;
import java.awt.Graphics;
import java.awt.Image;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

import javax.swing.ImageIcon;
import javax.swing.JFrame;

public class TelaInstrucoes extends JFrame{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	private desenhaTela Fundo = new desenhaTela();
	TelaInicial telaIni;

	public TelaInstrucoes(TelaInicial telaAnterior) {
		criaTela();	
		telaIni = telaAnterior;
	}
	
	public void iniciaTela() {
		this.setVisible(true);
		Fundo.paint(Fundo.getGraphics());
	}
	
	private void voltaTela() {
		this.setVisible(false);
		telaIni.setVisible(true);
	}
	
	private void criaTela() {
		this.setTitle("Dificuldades");
		this.setSize(TelaInicial.WIDTH_TELA_PADRAO, TelaInicial.HEIGHT_TELA_PADRAO);
		this.setLocationRelativeTo(null);
		this.setDefaultCloseOperation(EXIT_ON_CLOSE);
		this.setResizable(false);
		this.getContentPane().add("Center", Fundo);
		
		Fundo.addMouseListener(new MouseListener() {
			
			@Override
			public void mouseReleased(MouseEvent e) {
				 int x=e.getX();
			     int y=e.getY();		
			     
			     if ((x > 370 && x < 500) && (y > 1 && y < 50)) {
			    	voltaTela();
			     }
			}
			
			@Override
			public void mousePressed(MouseEvent e) {
			}
			
			@Override
			public void mouseExited(MouseEvent e) {
			}
			
			@Override
			public void mouseEntered(MouseEvent e) {				
			}
			
			@Override
			public void mouseClicked(MouseEvent e) {				
			}
		});
	}	
	
	private class desenhaTela extends Canvas {
		/**
		 * 
		 */
		private static final long serialVersionUID = 1L;

		@Override
		public void paint(Graphics g) {
			
			final Image imgFundo = new ImageIcon("imagens/Telas/TelaInfo.pngS").getImage(); 			
			g.drawImage(imgFundo, 0, 0,null);
		}
	}
}

