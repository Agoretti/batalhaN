import java.awt.Canvas;
import java.awt.Color;
import java.awt.Font;
import java.awt.Graphics;

import javax.swing.ImageIcon;
import javax.swing.JOptionPane;

import java.awt.Image;


public class CanvasJogo extends Canvas {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	public static final int LARGURA_CELULA = 60;
	public static final int ALTURA_CELULA = 60;
	public static final int LARGURA_BOTAO = 60;
	public static final int ALTURA_BOTAO = 60;
	public static final int LARGURA_PAINELHABILIDADES = 1;
	public static final int COINS_INICIAIS = 300;
	
	public static boolean jogando = false;
	
	private ReadTxt mapDados = new ReadTxt(1);
	private int qtdLinhas = mapDados.getLinhas();
	private int qtdColunas = mapDados.getColunas();
	private Embarcacoes barcos = mapDados.getEmbarcacoes();

	private int [][] matrizMira = new int[qtdLinhas][qtdColunas];
	private int [][] matrizClicks = new int[qtdLinhas][qtdColunas];
	private int [][] matrizJogo = mapDados.getMatriz();	
	Habilidades habilidades = new Habilidades(COINS_INICIAIS);
	
	private ImageIcon icon = new ImageIcon("imagens/Waves/ondas.jpg");
	private ImageIcon iconShot = new ImageIcon("imagens/Actions/explosion.png");
	private ImageIcon iconTiroAgua = new ImageIcon("imagens/Actions/tiroAgua.jpg");
	private ImageIcon iconMira = new ImageIcon("imagens/Actions/mira.png");
	private ImageIcon iconHabilidades = new ImageIcon("imagens/Fundos/chapaFundo.jpg");
	private ImageIcon iconDesligado = new ImageIcon("imagens/Actions/btnDesligado.png");
	private ImageIcon iconLigado = new ImageIcon("imagens/Actions/btnLigado.png");
	private ImageIcon iconJogar = new ImageIcon("imagens/Actions/btnJogar.jpg");
	private ImageIcon iconReiniciar = new ImageIcon("imagens/Actions/btnReiniciar.jpg");
	private ImageIcon iconPlaca1 = new ImageIcon("imagens/Fundos/placa1.jpg");
	private ImageIcon iconPlaca2 = new ImageIcon("imagens/Fundos/placa2.jpg");
	private ImageIcon iconPlaca3 = new ImageIcon("imagens/Fundos/placa3.jpg");
	private ImageIcon iconPlacaSaldo = new ImageIcon("imagens/Fundos/placaVazia.jpg");
	private ImageIcon iconX = new ImageIcon("imagens/Fundos/X.png");
	private static Font serifFont = new Font("Serif", Font.BOLD, 24);
	
	@Override
	public void paint(Graphics g) {
		
		final Image img = icon.getImage();
		final Image imgShot = iconShot.getImage();
		final Image imgTiroAgua = iconTiroAgua.getImage();
		final Image imgMira = iconMira.getImage();
		final Image imgFundo = iconHabilidades.getImage();
		final Image imgDesligado = iconDesligado.getImage();
		final Image imgLigado = iconLigado.getImage();
		final Image imgJogar = iconJogar.getImage();
		final Image imgReinicar = iconReiniciar.getImage();
		final Image imgPlaca1 = iconPlaca1.getImage();
		final Image imgPlaca2 = iconPlaca2.getImage();
		final Image imgPlaca3 = iconPlaca3.getImage();
		final Image imgPlacaSaldo = iconPlacaSaldo.getImage();
		final Image imgX = iconX.getImage();
		
		// Botao de start
		g.drawImage(imgFundo, qtdColunas*LARGURA_CELULA, 0, null);
		if (jogando) {
			g.drawImage(imgReinicar, qtdColunas*LARGURA_CELULA, 0, null);
		}else {
			g.drawImage(imgJogar, qtdColunas*LARGURA_CELULA, 0, null);
		}
		// Botoes de habilidades
	//	if (habilidades.getHabilidadeSelecionada() == habilidades.getIDCelula()) {
			g.drawImage(imgLigado, qtdColunas*LARGURA_CELULA+50, 90,LARGURA_BOTAO,ALTURA_BOTAO ,null);
	//	}else {
			g.drawImage(imgDesligado, qtdColunas*LARGURA_CELULA+50, 90,LARGURA_BOTAO,ALTURA_BOTAO ,null);
	//	}
		g.drawImage(imgPlaca1, qtdColunas*LARGURA_CELULA+130, 90, null);
//		if (habilidades.getHabilidadeSelecionada() == habilidades.getID2x2()) {
			g.drawImage(imgLigado, qtdColunas*LARGURA_CELULA+50, ALTURA_BOTAO+100,LARGURA_BOTAO,ALTURA_BOTAO ,null);
//		}else {
			g.drawImage(imgDesligado, qtdColunas*LARGURA_CELULA+50, ALTURA_BOTAO+100,LARGURA_BOTAO,ALTURA_BOTAO ,null);
//		}
		g.drawImage(imgPlaca2, qtdColunas*LARGURA_CELULA+130, ALTURA_BOTAO+100, null);
//		if (habilidades.getHabilidadeSelecionada() == habilidades.getIDLinhaColuna()) {
			g.drawImage(imgLigado, qtdColunas*LARGURA_CELULA+50, (2*ALTURA_BOTAO)+110,LARGURA_BOTAO,ALTURA_BOTAO ,null);
//		}else {
			g.drawImage(imgDesligado, qtdColunas*LARGURA_CELULA+50, (2*ALTURA_BOTAO)+110,LARGURA_BOTAO,ALTURA_BOTAO ,null);
//		}
//		if (habilidades.getCoins() < habilidades.getPrecoCelula()) {
			g.drawImage(imgX, qtdColunas*LARGURA_CELULA+10, 90,null);
//		}else {
			g.drawImage(null, qtdColunas*LARGURA_CELULA+10, 90 ,null);
//		}
//		if (habilidades.getCoins() < habilidades.getPreco2x2()) {
			g.drawImage(imgX, qtdColunas*LARGURA_CELULA+10, ALTURA_BOTAO+100 ,null);
//		}else {
			g.drawImage(null, qtdColunas*LARGURA_CELULA+50, ALTURA_BOTAO+100 ,null);
//		}
//		if (habilidades.getCoins() < habilidades.getPrecoLinhaColuna()) {
			g.drawImage(imgX, qtdColunas*LARGURA_CELULA+10, (2*ALTURA_BOTAO)+110 ,null);
//		}else {
			g.drawImage(null, qtdColunas*LARGURA_CELULA+10, (2*ALTURA_BOTAO)+110 ,null);
//		}
		// Placa de saldo
		g.drawImage(imgPlaca3, qtdColunas*LARGURA_CELULA+130, (2*ALTURA_BOTAO)+110, null);
		g.drawImage(imgPlacaSaldo, qtdColunas*LARGURA_CELULA,  (2*ALTURA_BOTAO)+180, null);
		g.setFont(serifFont);
	    g.setColor(Color.WHITE);
//	    g.drawString("Seu saldo: " + habilidades.getCoins() + " Coins", qtdColunas*LARGURA_CELULA+30, (2*ALTURA_BOTAO)+220);
		
		// Montagem do tabuleiro
		for(int i = 0; i < qtdLinhas; i++) {
			for(int j = 0; j < qtdColunas; j++) {				
				g.drawImage(img, j*LARGURA_CELULA, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
				if(matrizClicks[i][j] == 1) {
					if (matrizJogo[i][j] == 0) {
						g.drawImage(imgTiroAgua, j*LARGURA_CELULA, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
					}else {
						g.drawImage(imgShot, j*LARGURA_CELULA, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
					}
				}
				if ((matrizMira[i][j] == 1) && (matrizClicks[i][j] == 0)) {
					g.drawImage(imgMira, j*LARGURA_CELULA, i*ALTURA_CELULA, LARGURA_CELULA, ALTURA_CELULA, null);
				}
			}
		}	
	}

	public void setShot(int x, int y) {
		int celulasCertas = 0;
//		int IDHabilidade = habilidades.getHabilidadeSelecionada();
		
//		if (possuiSaldo(IDHabilidade)) {
//			habilidades.debitaCoins(IDHabilidade);
//			if (habilidades.getHabilidadeSelecionada() == habilidades.getIDCelula()) {
				if (matrizClicks[y][x] == 0) {
					matrizClicks[y][x] = 1;
				}
//			}else if (habilidades.getHabilidadeSelecionada() == habilidades.getID2x2()) {
				if ((y < qtdLinhas-1) && (x < qtdColunas-1)) {
					matrizClicks[y][x] = 1;
					matrizClicks[y][x+1] = 1;
					matrizClicks[y+1][x] = 1;
					matrizClicks[y+1][x+1] = 1;
				}
//			}else if (habilidades.getHabilidadeSelecionada() == habilidades.getIDLinhaColuna()) {
				for (int i=0; i< qtdLinhas; i++) {
					for (int j=0; j < qtdColunas; j++) {
						if (i == y || j == x) {
							matrizClicks[i][j] = 1;
						}
					}
				}
			//}
	//	}
		
		//Verficação de vitória ou perda do jogo
//		if (habilidades.getCoins() == 0) {
			janelaMensagem("Que pena, voce perdeu!");
			reiniciaJogo();
//		}
		for(int i=0;i < qtdLinhas; i++) {
			for(int j=0;j < qtdColunas; j++) {
				if((matrizJogo[i][j] != 0) && matrizClicks[i][j] == 1) {
					celulasCertas += 1;
				}
			}
		}
		if (celulasCertas == barcos.getQtdCelulas()) {
			janelaMensagem("Parabéns marinheiro, você ganhou!");
			reiniciaJogo();
		}
	}
	
	public boolean possuiSaldo(int IDHabilidade) {
//		if ( habilidades.getCoins() >= habilidades.getPrecos(IDHabilidade)) {
			return true;
//		}else {
//			return false;
//		}
	}
	
	private void janelaMensagem(String msg) {
		JOptionPane.showMessageDialog(this, msg, "Batalha Naval", JOptionPane.ERROR_MESSAGE);
	}
	
	public void marcaHabilidade(int IDHabilidade) {
		if (possuiSaldo(IDHabilidade)) {
//			if (IDHabilidade == habilidades.getIDCelula()) {
//				habilidades.selecionaAtacarCelula();
//			}else if(IDHabilidade == habilidades.getID2x2()) {
//				habilidades.selecionaAtacar2x2();
//			}else if (IDHabilidade == habilidades.getIDLinhaColuna()) {
//				habilidades.selecionaAtacarLinhaColuna();
//			}
		}
	}
	
	public void setMira(int x, int y) {
//		if (habilidades.getHabilidadeSelecionada() == habilidades.getIDCelula()) {
			for (int i=0; i< qtdLinhas; i++) {
				for (int j=0; j < qtdColunas; j++) {
					matrizMira[i][j] = 0;
				}
			}
			matrizMira[y][x] = 1;
//		}else if (habilidades.getHabilidadeSelecionada() == habilidades.getID2x2()) {
			for (int i=0; i< qtdLinhas; i++) {
				for (int j=0; j < qtdColunas; j++) {
					matrizMira[i][j] = 0;
				}
			}
			if ((y < qtdLinhas-1) && (x < qtdColunas-1)) {
				matrizMira[y][x] = 1;
				matrizMira[y][x+1] = 1;
				matrizMira[y+1][x] = 1;
				matrizMira[y+1][x+1] = 1;
			}
//		}else if (habilidades.getHabilidadeSelecionada() == habilidades.getIDLinhaColuna()) {
			for (int i=0; i< qtdLinhas; i++) {
				for (int j=0; j < qtdColunas; j++) {
					if (i == y || j == x) {
						matrizMira[i][j] = 1;
					}else {
						matrizMira[i][j] = 0;
					}
				}
			}
//		}
	}
	
	public void reiniciaJogo() {
		for(int i=0; i < qtdLinhas; i++) {
			for(int j=0; j < qtdColunas; j++) {
				matrizClicks[i][j] = 0;
				matrizMira[i][j] = 0;
			}
		}
//		habilidades.desativaHabilidades(COINS_INICIAIS);
		jogando = false;
	}
	
	public int getQtdColunas() {
		return qtdColunas;
	}

	public int getQtdLinhas() {
		return qtdLinhas;
	}
}
