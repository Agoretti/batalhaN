
public class Tabuleiro {

	private int [][] matrizClicks;
	private int [][] matrizMira;
	private int [][] matrizNavios;
	
	public Tabuleiro(int linhas, int colunas) {
		matrizClicks = new int[linhas][colunas];
		matrizMira = new int[linhas][colunas];
		matrizNavios = new int[linhas][colunas];
	}
	
	public int[][] getMatrizClicks() {
		return matrizClicks;
	}
	public int[][] getMatrizNavios() {
		return matrizNavios;
	}
	public int[][] getMatrizMira() {
		return matrizMira;
	}
	
	private boolean posicaoValida(int[][]matriz,int linha, int coluna) {
		if (linha < 0 || coluna < 0) {
			return false;
		}else {
			
			int y = 0;
			int x = 0;    
			if (matriz.length > 0)
			{
			    y = matriz.length;
			    x = matriz[0].length;  
			  
			    if (linha <= y-1 && coluna <= x-1) {
					return true;
				}else {
					return false;
				}
			}else {
				return false;
			}
		}
	}
	 
	public boolean matrizesIguais(int[][]matriz1, int[][]matriz2) {
		int y1 = 0;
		int x1 = 0;
		int y2 = 0;
		int x2 = 0;
		if (matriz1.length > 0 && matriz2.length > 0)
		{
		    y1 = matriz1.length;
		    x1 = matriz1[0].length;  
		    y2 = matriz1.length;
		    x2 = matriz1[0].length;
		    
		    if (y1 == y2 && x1 == x2) {
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
		
	}
	
	public boolean setMatrizNavios(int[][] matriz) {
		if(!matrizesIguais(matrizNavios, matriz)) {
			return false;
		}else {
			this.matrizNavios = matriz;
			return true;
		}
	}
	
	public boolean marcaCelula(int linha, int coluna) {
		if (posicaoValida(matrizClicks, linha, coluna)) {
			matrizClicks[linha][coluna] = 1;
			return true;
		}else {
			return false;
		}
	}
	
	public boolean miraCelula(int linha, int coluna) {
		if (posicaoValida(matrizMira, linha, coluna)) {
			for (int i=0; i< qtdLinhasMatriz(matrizMira); i++) {
				for (int j=0; j < qtdColunasMatriz(matrizMira); j++) {
					matrizMira[i][j] = 0;
				}
			}
			matrizMira[linha][coluna] = 1;
			return true;
		}else {
			return false;
		}
	}
	
	public boolean marcaCelula2x2(int linha, int coluna) {
		if (posicaoValida(matrizClicks, linha, coluna)) {
			if (linha < (qtdLinhasMatriz(matrizClicks)-1) && coluna < (qtdColunasMatriz(matrizClicks)-1)) {
				matrizClicks[linha][coluna] = 1;
				matrizClicks[linha][coluna+1] = 1;
				matrizClicks[linha+1][coluna] = 1;
				matrizClicks[linha+1][coluna+1] = 1;
			return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public boolean miraCelula2x2(int linha, int coluna) {
		int qtdLinhas = qtdLinhasMatriz(matrizMira);
		int qtdColunas = qtdColunasMatriz(matrizMira);
		if(posicaoValida(matrizMira, linha, coluna)) {
			for (int i=0; i< qtdLinhas; i++) {
				for (int j=0; j < qtdColunas; j++) {
					matrizMira[i][j] = 0;
				}
			}
			if ((linha < qtdLinhas-1) && (coluna < qtdColunas-1)) {
				matrizMira[linha][coluna] = 1;
				matrizMira[linha][coluna+1] = 1;
				matrizMira[linha+1][coluna] = 1;
				matrizMira[linha+1][coluna+1] = 1;
				return true;
			}else {
				return false;
			}
		}else {
			return false;
		}
	}
	
	public boolean marcaLinhaColuna(int linha, int coluna) {
		if (posicaoValida(matrizClicks, linha, coluna)) {
			for (int i=0; i< qtdLinhasMatriz(matrizClicks); i++) {
				for (int j=0; j < qtdColunasMatriz(matrizClicks); j++) {
					if (i == linha || j == coluna) {
						matrizClicks[i][j] = 1;
					}
				}
			}
			return true;
		}else {
			return false;
		}
	}
	
	public boolean miraLinhaColuna(int linha, int coluna) {
		if (posicaoValida(matrizMira, linha, coluna)) {
			for (int i=0; i< qtdLinhasMatriz(matrizMira); i++) {
				for (int j=0; j < qtdColunasMatriz(matrizMira); j++) {
					if (i == linha || j == coluna) {
						matrizMira[i][j] = 1;
					}else {
						matrizMira[i][j] = 0;
					}
				}
			}
			return true;
		}else {
			return false;
		}
	}
	
	public int qtdLinhasMatriz(int[][] matriz) {
		int y = 0;
		if (matriz.length > 0)
		{
		    y = matriz.length;   
		}
		return y;
	}
	
	public int qtdColunasMatriz(int[][]matriz) {
		int x = 0;
		if (matriz.length > 0)
		{
		    x = matriz[0].length;            
		}
		return x;
	}
}
