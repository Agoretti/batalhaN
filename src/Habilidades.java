
public class Habilidades {
	
	public static final int ID_ATACAR_UMA_POSICAO = 1;
	public static final int ID_ATACAR_AREA_2x2 = 2;
	public static final int ID_ATACAR_LINHA_COLUNA = 3;
	
	private Habilidade atacarUmaPosicao;
	private Habilidade atacarArea2x2;
	private Habilidade atacarLinhaColunas;
	private Recursos saldoJogo;
	
	
	public Habilidades(int saldoInicial) {
		atacarUmaPosicao = new Habilidade(ID_ATACAR_UMA_POSICAO, "Uma Posicao", 10, false);
		atacarArea2x2 = new Habilidade(ID_ATACAR_AREA_2x2, "Area 2x2", 20, false);
		atacarLinhaColunas = new Habilidade(ID_ATACAR_LINHA_COLUNA, "Linha Coluna", 50, false);
		saldoJogo = new Recursos(saldoInicial);
	}
	
	public int IDHabilidadeSelecionada() {
		if (atacarUmaPosicao.getSelecionada()) {
			return Habilidades.ID_ATACAR_UMA_POSICAO;
		}else if(atacarArea2x2.getSelecionada()) {
			return Habilidades.ID_ATACAR_AREA_2x2;
		}else if(atacarLinhaColunas.getSelecionada()){
			return Habilidades.ID_ATACAR_LINHA_COLUNA;
		}else {
			return 0;
		}
	}
	
	public Habilidade HabilidadeSelecionada() {
		if (atacarUmaPosicao.getSelecionada()) {
			return atacarUmaPosicao;
		}else if(atacarArea2x2.getSelecionada()) {
			return atacarArea2x2;
		}else if(atacarLinhaColunas.getSelecionada()){
			return atacarLinhaColunas;
		}else {
			return null;
		}
	}
	
	public void marcaHabilidade(Habilidade habSelecionada) {
		if (saldoJogo.possuiSaldo(habSelecionada.getPreco())) {
			if(habSelecionada.getID() == Habilidades.ID_ATACAR_UMA_POSICAO) {
				atacarUmaPosicao.setSelecionado(true);
				atacarArea2x2.setSelecionado(false);
				atacarLinhaColunas.setSelecionado(false);
			}else if(habSelecionada.getID() == Habilidades.ID_ATACAR_AREA_2x2) {
				atacarUmaPosicao.setSelecionado(false);
				atacarArea2x2.setSelecionado(true);
				atacarLinhaColunas.setSelecionado(false);
			}else if(habSelecionada.getID() == Habilidades.ID_ATACAR_LINHA_COLUNA) {
				atacarUmaPosicao.setSelecionado(false);
				atacarArea2x2.setSelecionado(false);
				atacarLinhaColunas.setSelecionado(true);
			}
		}
	}
	
	public Recursos getSaldoJogo() {
		return this.saldoJogo;
	}
	
	public void marcaHabilidade(int IDHabSelecionada) {
		int precoTemp = 0;
		if (IDHabSelecionada == ID_ATACAR_UMA_POSICAO) {
			precoTemp = atacarUmaPosicao.getPreco();
		}else if (IDHabSelecionada == ID_ATACAR_AREA_2x2) {
			precoTemp = atacarArea2x2.getPreco();
		}else if (IDHabSelecionada == ID_ATACAR_LINHA_COLUNA) {
			precoTemp = atacarLinhaColunas.getPreco();
		}
		if (saldoJogo.possuiSaldo(precoTemp)) {
			if(IDHabSelecionada == Habilidades.ID_ATACAR_UMA_POSICAO) {
				atacarUmaPosicao.setSelecionado(true);
				atacarArea2x2.setSelecionado(false);
				atacarLinhaColunas.setSelecionado(false);
			}else if(IDHabSelecionada == Habilidades.ID_ATACAR_AREA_2x2) {
				atacarUmaPosicao.setSelecionado(false);
				atacarArea2x2.setSelecionado(true);
				atacarLinhaColunas.setSelecionado(false);
			}else if(IDHabSelecionada == Habilidades.ID_ATACAR_LINHA_COLUNA) {
				atacarUmaPosicao.setSelecionado(false);
				atacarArea2x2.setSelecionado(false);
				atacarLinhaColunas.setSelecionado(true);
			}
		}
	}

}
